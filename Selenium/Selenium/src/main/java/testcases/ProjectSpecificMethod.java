package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import wdMethods.SeMethods;

public class ProjectSpecificMethod  extends SeMethods{

	@Parameters({"browser","url","username","password"})
	@BeforeMethod(groups="common")
	public void doLogin(String browsername,String url,String uname,String pwrd)
	{
        startApp(browsername,url);
		
		WebElement username = locateElement("id", "username");
		
		WebElement password = locateElement("id", "password");
		
		type(username, uname);
		
		type(password, pwrd);
		
		WebElement loginbutton = locateElement("classname","decorativeSubmit");
		
		click(loginbutton);
		
	}
	@AfterMethod(groups="common")
	public void closeApp()
	{
	  closeBrowser();
		
	}
	
}
