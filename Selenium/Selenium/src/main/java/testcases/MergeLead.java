package testcases;

import java.util.Scanner;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class MergeLead extends ProjectSpecificMethod {
	
	@BeforeClass(groups="common")
	public void SetData() {
		testcaseName= "TC05_MergeLead";
		testDesc = "Merge two Leads";
		author = "Kiran";
		category  = "Sanity";
		
	}
	
	
//@Test(enabled=false)
	//@Test(groups="Regression", dependsOnGroups="Sanity")
	@Test (dataProvider="test4")
public void mergeleadtest(String id1,String id2) throws InterruptedException
 {
	
	WebElement crmbutton = locateElement("linktext","CRM/SFA");
	
	click(crmbutton);
	
	WebElement leadlink = locateElement("xpath","//a[.='Leads']");
	
	click(leadlink);
	
	WebElement mergeleads = locateElement("xpath", "//a[.='Merge Leads']");
	
	click(mergeleads);
	
	WebElement lookup1 = locateElement("xpath","//table[@name='ComboBox_partyIdFrom']/following::img[@alt='Lookup'][1]");
	
	click(lookup1);
	
	switchToWindow(1);
	
	WebElement leadID1 = locateElement("xpath","//input[@name='id']");
	
	type(leadID1, id1);
	
	WebElement findleadsbutton1 = locateElement("xpath","//button[.='Find Leads']");
	
	click(findleadsbutton1);
	
	Thread.sleep(3000);
	
	WebElement firstrowlead = locateElement("xpath","//table[@class='x-grid3-row-table']//tr[1]/td[1]/div/a");
	
	click(firstrowlead);
	
	switchToWindow(0);
	
	WebElement lookup2 = locateElement("xpath","//table[@name='ComboBox_partyIdFrom']/following::img[@alt='Lookup'][2]");
	
	click(lookup2);
	
	switchToWindow(1);
	
	WebElement leadID2 = locateElement("xpath","//input[@name='id']");
	
	type(leadID2, id2);
	
	WebElement findleadsbutton2 = locateElement("xpath","//button[.='Find Leads']");
	
	click(findleadsbutton2);
	
	Thread.sleep(3000);
	
	WebElement firstrowID2 = locateElement("xpath","//table[@class='x-grid3-row-table']//tr[1]/td[1]/div/a");
	
	click(firstrowID2);
	
	switchToWindow(0);
	
	WebElement mergebutton = locateElement("xpath","//a[.='Merge']");
	
	click(mergebutton);
	
	acceptAlert();
	
	Thread.sleep(3000);
	
	
  }
	
	@DataProvider(name="test4")
	public String[][] data4(){
		
		String str4[][]=new String[1][2];
		str4[0][0]="10048";
		str4[0][1]="10049";
		
		return str4;
		
	}
	
	
}
