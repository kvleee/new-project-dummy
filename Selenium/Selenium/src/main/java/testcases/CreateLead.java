package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class CreateLead extends ProjectSpecificMethod {

	@BeforeClass(groups="common")
	public void SetData()
	{
		testcaseName= "TC01_CreateLead";
		testDesc = "Create a lead with mandate values";
		author = "Kiran";
		category  = "Smoke";

	}
	
	
	//@Test(/*invocationCount=2/*invocationTimeOut=20000*/)
	//@Test(groups="Smoke")

	@Test (dataProvider="test1")
	public void createleadtest(String company,String fname,String lname)
	{

		WebElement crmbutton = locateElement("linktext","CRM/SFA");
		
		click(crmbutton);
		
		WebElement leadlink = locateElement("xpath","//a[.='Leads']");
		
		click(leadlink);
		
		WebElement createleadlink = locateElement("xpath","//a[.='Create Lead']");
		
		click(createleadlink);
		
		WebElement companyname = locateElement("xpath","//input[@name='companyName'][@class='inputBox']");
		
		type(companyname, company);
		
		WebElement firstname = locateElement("xpath", "//input[@name='firstName'][@class='inputBox']");
		
		type(firstname, fname);
		
		WebElement lastname = locateElement("xpath","//input[@name='lastName'][@class='inputBox']");
		
		type(lastname, lname);
		
		WebElement createleadbutton = locateElement("xpath","//input[@value='Create Lead']");
		
		click(createleadbutton);
	
	}
	@DataProvider(name="test1")
	public  String[][] data(){
		
		String ss[][]=new String[2][3];
		
		ss[0][0]="EZLynx";
		ss[0][1]="kiran kumaar";
		ss[0][2]="venkatesan";
		
		ss[1][0]="cts";
		ss[1][1]="ramesh";
		ss[1][2]="kumar";
		
		return ss;
		
	}
	
}
