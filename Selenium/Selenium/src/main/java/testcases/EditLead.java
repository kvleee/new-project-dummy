package testcases;

import java.util.Scanner;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class EditLead extends ProjectSpecificMethod{

	@BeforeClass(groups="common")
	public void SetData() {
		testcaseName= "TC04_EditLead";
		testDesc = "Edit the Lead";
		author = "Kiran";
		category  = "Regression";
		
	}
	
	
	//@Test(dependsOnMethods="testcases.CreateLead.createleadtest")
	//@Test(groups="Sanity",dependsOnGroups="Smoke")
	@Test(dataProvider="test2")
	public void editleadtest(String fname,String cmpany) throws InterruptedException
	{
		
		WebElement crmbutton = locateElement("linktext","CRM/SFA");
		
		click(crmbutton);
		
        WebElement leadlink = locateElement("xpath","//a[.='Leads']");
		
		click(leadlink);
		
		WebElement findlead = locateElement("xpath","//a[.='Find Leads']");
		
		click(findlead);
		
		WebElement firstname = locateElement("xpath", "//div[.='Find Leads']/following::label[.='First name:']/../div/input");
		
		type(firstname, fname);
		
		WebElement findleads = locateElement("xpath","//button[.='Find Leads']");
		
		click(findleads);
		
		Thread.sleep(3000);
		
		WebElement firstrowlead = locateElement("xpath","//span[.='Lead List']/following::table[2]//tr[1]/td[1]//a");
		
		click(firstrowlead);
		
		WebElement edit = locateElement("xpath","//a[.='Edit']");
		
		click(edit);
		
		WebElement companynametx = locateElement("xpath","//table//input[@name='companyName']");

		type(companynametx, cmpany);

		WebElement update = locateElement("xpath","//input[@value='Update']");
		
		click(update);
		
		WebDriverWait wait=new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(locateElement("xpath","//div[.='View Lead']")));
		
	   WebElement companyname = locateElement("xpath","//div[.='View Lead']/following::div[.='Lead']/following::table//span[.='Company Name']/../../td[2]/span");
		
	   verifyPartialText(companyname, cmpany);
 	
	}
	
	@DataProvider(name="test2")
   public String[][] data2(){
	   
	   String ss2[][]=new String [2][2];
	   
	   ss2[0][0]="kiran kumaar";
	   ss2[0][1]="Infosys";
	   
	   ss2[1][0]="ramesh";
	   ss2[1][1]="jpmorgan";
	   
	   return ss2;
	   
   }
	
	
}
