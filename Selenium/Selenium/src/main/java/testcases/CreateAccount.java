package testcases;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CreateAccount extends ProjectSpecificMethod{
	@BeforeClass
	public void SetData() {
		testcaseName= "TC06_CreateAccount";
		testDesc = "Create account";
		author = "Kiran";
		category  = "Smoke";
		
		
	}
	@Test
	public void createaccounttest() throws InterruptedException {
		
		click(locateElement("linktext","CRM/SFA"));
		click(locateElement("xpath","//a[.='Accounts']"));
		click(locateElement("xpath","//a[.='Create Account']"));
        type(locateElement("xpath","//input[@id='accountName']"),"kkbtestaccount");
		selectDropDownUsingText(locateElement("xpath","//select[@name='industryEnumId']"),"Computer Software");
		selectDropDownUsingText(locateElement("xpath","//select[@id='currencyUomId']"),"INR - Indian Rupee");
		selectDropDownUsingText(locateElement("xpath","//select[@id='dataSourceId']"),"Cold Call");
		selectDropDownUsingText(locateElement("xpath","//select[@id='marketingCampaignId']"), "Automobile");
		type(locateElement("xpath","//input[@id='primaryPhoneNumber']"),"7845328056");
		type(locateElement("xpath","//input[@id='generalCity']"),"Hosur");
		type(locateElement("xpath","//input[@id='primaryEmail']"),"kiran13.94@gmail.com");
        selectDropDownUsingText(locateElement("xpath","//select[@id='generalCountryGeoId']"), "India");
        selectDropDownUsingText(locateElement("xpath","//select[@id='generalStateProvinceGeoId']"),"KARNATAKA");
        click(locateElement("xpath","//input[@value='Create Account']"));
        Thread.sleep(3000);
        WebDriverWait wait1=new WebDriverWait(driver, 30);
        wait1.until(ExpectedConditions.visibilityOf(locateElement("xpath","//span[.='Account Name']/../following::td[1]/span")));
        String accID=getText(locateElement("xpath","//span[.='Account Name']/../following::td[1]/span")).replaceAll("[^0-9]","");
        click(locateElement("xpath","//a[.='Find Accounts']"));
        type(locateElement("xpath","//input[@name='id']"),accID);
        type(locateElement("xpath","//span[.='Find by']/following::input[@name='accountName']"),"kirantestaccount");
        click(locateElement("xpath","//button[.='Find Accounts']"));
        Thread.sleep(3000);
        WebDriverWait wait=new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.elementToBeClickable(locateElement("xpath","//table[@class='x-grid3-row-table']//tr[1]/td[1]/div/a")));
         
       verifyExactText(locateElement("xpath","//table[@class='x-grid3-row-table']//tr[1]/td[1]/div/a"), accID);
        
        
        
        
        
        
		
		
		
		
		
		
	}
	
	
}
