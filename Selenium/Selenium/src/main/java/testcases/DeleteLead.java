package testcases;

import java.util.Scanner;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class DeleteLead extends ProjectSpecificMethod{

	@BeforeClass
	public void SetData() {
		testcaseName= "TC02_DeleteLead";
		testDesc = "Delete the Lead";
		author = "Kiran";
		category  = "Sanity";

	}

	@Test(dataProvider="test5")
	public void deleteleadtest(String phno) throws InterruptedException
	{

		WebElement crmbutton = locateElement("linktext","CRM/SFA");
		
		click(crmbutton);
		
        WebElement leadlink = locateElement("xpath","//a[.='Leads']");
		
		click(leadlink);
		
		WebElement findlead = locateElement("xpath","//a[.='Find Leads']");
		
		click(findlead);
		
		WebElement phonetab = locateElement("xpath","(//span[.='Find by']/following::div//span[.='Phone'])[2]");
		
		click(phonetab);
		
		WebElement phoneTF = locateElement("xpath","//input[@name='phoneNumber']");
		
		type(phoneTF, phno);
		
		WebElement findleadsbutton = locateElement("xpath","//button[.='Find Leads']");
		
		click(findleadsbutton);
		
		Thread.sleep(3000);
		
		WebElement firstrowlead = locateElement("xpath","//span[.='Lead List']/following::table[2]//tr[1]/td[1]//a");
		
		String actualfirstrowtext=getText(firstrowlead);
		
		click(firstrowlead);
		
		WebElement deletelead = locateElement("xpath","//a[.='Delete']");
		
		click(deletelead);
		
		WebDriverWait wait=new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.titleContains("My Leads"));

		WebElement findleads2 = locateElement("xpath","//a[.='Find Leads']");

		click(findleads2);
		
		WebElement leadidTF = locateElement("xpath","//input[@name='id']");
		
		type(leadidTF, actualfirstrowtext);
		
		WebElement findleadsbutton2 = locateElement("xpath","//button[.='Find Leads']");
		click(findleadsbutton2);
		
		WebElement errmsg = locateElement("xpath","(//span[.='Lead List']/following::table/following::div)[8]");
		
		verifyDisplayed(errmsg);
		

	}
	
	@DataProvider(name="test5")
	public String[][] data5(){
		
		String[][] str5=new String[2][1];
		str5[0][0]="7845328056";
		str5[1][0]="823456788";
		
		return str5;
		
		
	}
	
	
}
