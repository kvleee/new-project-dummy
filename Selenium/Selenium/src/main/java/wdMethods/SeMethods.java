package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.UnexpectedTagNameException;

import utils.BasicReport;

public class SeMethods extends BasicReport implements WdMethods{
	public RemoteWebDriver driver;	
	public int i = 1;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./driver/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//System.out.println("The Browser "+browser+" Launched Successfully");
			
			logStep("The Browser "+browser+" Launched Successfully", "PASS");
			
		} catch (IllegalStateException e) {
			//System.err.println("The Browser "+browser+" not Launched Successfully");
			logStep("The Browser "+browser+" not Launched Successfully", "FAIL");
			throw new RuntimeException(e.getMessage());	
			
		}
		catch (WebDriverException e) {
			//System.err.println("The Browser "+browser+" not Launched Successfully");
			logStep("The Browser "+browser+" not Launched Successfully", "FAIL");
			throw new RuntimeException(e.getMessage());
		}
		finally {
			takeSnap();
		}
		
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch (locator) {
			case "id": return driver.findElementById(locValue);
			case "classname": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "linktext": return driver.findElement(By.linkText(locValue));
			case "tagname":return driver.findElement(By.tagName(locValue));
			case "name": return driver.findElement(By.name(locValue));
			case  "partiallinktext":return driver.findElement(By.partialLinkText(locValue));
			case "cssselector":return driver.findElement(By.cssSelector(locValue));
			
			
			}
		} catch (NoSuchElementException e) {
			//System.err.println("The element is not located");
			logStep("The element is not locate", "FAIL");
			throw new RuntimeException(e.getMessage());
		}
		
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		
		try {
			WebElement element = driver.findElement(By.id(locValue));
			logStep("The element is  located", "PASS");
			return element;
		} catch (NoSuchElementException e) {
			//System.err.println("The element is not located");
			logStep("The element is not locate", "FAIL");
			
		}
		
		return null;
		
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			//System.out.println("The Data "+data+" Entered Successfully");
			logStep("The Data "+data+" Entered Successfully", "PASS");
		} catch (InvalidElementStateException e) {
			//System.err.println("The"+ele+"is disabled");
			logStep("The"+ele+"is disabled", "FAIL");
			throw new RuntimeException(e.getMessage());
			
		}
		finally {
			takeSnap();
		}
		
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			//System.out.println("The Element "+ele+" is clicked Successfully");
			logStep("The Element "+ele+" is clicked Successfully", "PASS");
		} catch (InvalidElementStateException e) {
			//System.err.println("The "+ele+"is disabled");
			logStep("The "+ele+"is disabled", "FAIL");
			throw new RuntimeException(e.getMessage());
		}
		
		
	}

	@Override
	public String getText(WebElement ele) {
		
		try {
			String text = ele.getText();
			logStep("The text found", "PASS");
			return text;
		} catch (StaleElementReferenceException e) {
			//System.err.println("The object address not found");
			logStep("The text not found", "FAIL");
			throw new RuntimeException(e.getMessage());
			
		}
		catch (WebDriverException e) {
			//System.err.println("The text not found");
			logStep("The text not found", "FAIL");
			throw new RuntimeException(e.getMessage());
			
		}
		
	}

	public void selectDropDownUsingText(WebElement ele, String value) {

       try {
		Select select=new Select(ele);
		   select.selectByVisibleText(value);
		   //System.out.println("dropdown "+value+" selected");
		   logStep("dropdown "+value+" selected", "PASS");
	} catch (UnexpectedTagNameException e) {
		//System.err.println("The dropdown type is not select");
		logStep("The dropdown type is not select", "FAIL");
		throw new RuntimeException(e.getMessage());
	}
	finally {
		takeSnap();
	}	
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select select=new Select(ele);
			select.selectByIndex(index);
			//System.out.println("dropdown selected using index");
			logStep("dropdown selected using index", "PASS");
		} catch (UnexpectedTagNameException e) {
			//System.err.println("The dropdown type is not select");
			logStep("The dropdown type is not select", "FAIL");
			throw new RuntimeException(e.getMessage());
		}
		catch (IndexOutOfBoundsException e) {
			//System.err.println("The index out of bound");
			logStep("The index out of bound", "FAIL");
			throw new RuntimeException(e.getMessage());
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		
			try {
				String actualtitle=driver.getTitle();
				logStep("The title found", "PASS");
				return actualtitle.equalsIgnoreCase(expectedTitle);
			} catch (NoSuchWindowException e) {
				//System.err.println("The browser not opened");
				logStep("The browser not opened", "FAIL");
				throw new RuntimeException(e.getMessage());
				
			}
			catch (WebDriverException e) {
				//System.err.println("title not found");
				logStep("title not found", "FAIL");
				throw new RuntimeException(e.getMessage());
				
			}
		
		
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String actualtext=ele.getText();
			if(actualtext.equalsIgnoreCase(expectedText))
			{
				//System.out.println("verifyExactText----passed");
				logStep("verifyExactText----passed", "PASS");
			}
			else
			{
				//System.err.println("verifyExactText----failed");
				logStep("verifyExactText----failed", "FAIL");
			}
		} catch (InvalidElementStateException e) {
			//System.err.println("The element is disabled");
			logStep("The element is disabled", "FAIL");
			throw new RuntimeException(e.getMessage());
			
		}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String actualText=ele.getText();
			if(actualText.contains(expectedText))
			{
				//System.out.println("verifyPartialText----passed");
				logStep("verifyPartialText----passed", "PASS");
				
			}
			else
			{
				//System.out.println("verifyPartialText----failed");
				logStep("verifyPartialText----failed", "FAIL");
			}
		} catch (InvalidElementStateException e) {
			//System.err.println("The element is disabled");
			logStep("The element is disabled", "FAIL");
			throw new RuntimeException(e.getMessage());
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
			String actual=ele.getAttribute(attribute);
			 if(actual.equalsIgnoreCase(value))
			 {
				 //System.out.println("verifyExactAttribute---passed");
				 logStep("verifyExactAttribute---passed", "PASS");
			 }
			 else
			 {
				 //System.out.println("verifyExactAttribute---failed");
				 logStep("verifyExactAttribute---failed", "FAIL");
				 
			 }
		} catch (InvalidElementStateException e) {
			//System.err.println("The element is disabled");
			 logStep("The element is disabled", "FAIL");
			throw new RuntimeException(e.getMessage());
			
			
		}
		
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			String actual=ele.getAttribute(attribute);
			if(actual.contains(value))
			{
			 //System.out.println("verifyPartialAttribute---passed");
				logStep("verifyPartialAttribute---passed", "PASS");
			}
			else
			{
			 //System.out.println("verifyPartialAttribute---failed");
				logStep("verifyPartialAttribute---failed", "FAIL");
			}
		} catch (InvalidElementStateException e) {
			//System.err.println("The element is disabled");
			logStep("The element is disabled", "FAIL");
			throw new RuntimeException(e.getMessage());
		}

	}

	@Override
	public void verifySelected(WebElement ele) {
		if(ele.isSelected()) {
			
			//System.out.println(ele+" is selected");
			logStep(ele+" is selected", "PASS");
		}
		else
		{
			//System.out.println(ele+" is not selected");
			logStep(ele+" is not selected", "FAIL");
		}
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
       try {
		if(ele.isDisplayed()) {
				
				//System.out.println(ele+" is displayed");
			logStep(ele+" is displayed", "PASS");
			}
			else
			{
				//System.out.println(ele+" is not displayed");
				logStep(ele+" is not displayed", "FAIL");
			}
	} catch (NoSuchElementException e) {
		logStep(ele+" is not present", "FAIL");
		throw new RuntimeException(e.getMessage());
	}
       catch (ElementNotVisibleException e) {
    	   logStep(ele+" is not visible", "FAIL");  
		throw new RuntimeException(e.getMessage());
	}

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allwindow = driver.getWindowHandles();
			List<String> lst=new ArrayList<>();
			lst.addAll(allwindow);
			driver.switchTo().window(lst.get(index));
			//System.out.println("switched to window successfully");
			logStep("switched to window successfully", "PASS"); 
		} catch (IndexOutOfBoundsException e) {
			//System.err.println("index out of bound");
			logStep("index out of bound", "FAIL");
			throw new RuntimeException(e.getMessage());
		}
		catch (NoSuchWindowException e) {
			//System.err.println("window not presentr");
			logStep("window not present", "FAIL");
			throw new RuntimeException(e.getMessage());
		}
		
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			//System.out.println("switched successfully to frame");
			logStep("switched successfully to frame", "PASS");
		} catch (NoSuchFrameException e) {
			//System.err.println("Frame not present");
			logStep("Frame not present", "FAIL");
			throw new RuntimeException(e.getMessage());
		}

	}

	@Override
	public void acceptAlert() {
		try {
			Alert a = driver.switchTo().alert();
			a.accept();
			//System.out.println("Alert Accepted");
			logStep("Alert Accepted", "PASS");
		} catch (NoAlertPresentException e) {
			//System.err.println("alert not present");
			logStep("alert not present", "FAIL");
			throw new RuntimeException(e.getMessage());
		}

	}

	@Override
	public void dismissAlert() {
		try {
			Alert a = driver.switchTo().alert();
			a.dismiss();
			//System.out.println("Alert dismissed");
			logStep("Alert dismissed", "PASS");
		} catch (NoAlertPresentException e) {
			//System.err.println("alert not present");
			logStep("alert not present", "FAIL");
			throw new RuntimeException(e.getMessage());
		}

	}

	@Override
	public String getAlertText() {
	    try {
			Alert a = driver.switchTo().alert();
			String alerttext=a.getText();
			logStep("alert present", "PASS");
			return alerttext;
		} catch (NoAlertPresentException e) {
			//System.err.println("No alert present");
			logStep("alert not present", "FAIL");
			throw new RuntimeException(e.getMessage());
		}
	}


	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, desc);
			logStep("snapshot taken", "PASS");
		} catch (IOException e) {
			logStep("snapshot not taken", "FAIL");
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		try {
			driver.close();
			//System.out.println("The current browser closed successfully");
			logStep("The current browser closed successfully", "PASS");
		} catch (NoSuchWindowException e) {
			//System.err.println("No browser present");
			logStep("No browser present", "FAIL");
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public void closeAllBrowsers() {
		try {
			driver.quit();
      //System.out.println("All browsers closed succefully");
			logStep("All browsers closed succefully", "PASS");
		} catch (NoSuchWindowException e) {
			//System.err.println("No browser present");
			logStep("No browser present", "FAIL");
			throw new RuntimeException(e.getMessage());
		}
	}
	
}
