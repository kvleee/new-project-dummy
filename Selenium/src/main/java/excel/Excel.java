package excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {

	public static Object[][] excelData(String filename) throws IOException {
		
		XSSFWorkbook wbook= new XSSFWorkbook("./data/"+filename+".xlsx");
		//XSSFSheet sheet= wbook.getSheet("CreateLead");
		XSSFSheet sheet= wbook.getSheetAt(0);
		int lastRowNum = sheet.getLastRowNum();
		System.out.println("Row count is " +lastRowNum);
		int rowCount = sheet.getRow(0).getLastCellNum();
		System.out.println("Coulmn count is " +rowCount);
		Object[][] data = new Object[lastRowNum][rowCount];
		
		for (int i = 1; i<=lastRowNum; i++) {
			XSSFRow row = sheet.getRow(i);
			
			for (int j = 0;j< rowCount; j++) {
				XSSFCell cell = row.getCell(j);
				String stringCellValue = cell.getStringCellValue();
				data[i-1][j] = stringCellValue; //Y this is used here need to understand the flow (array will start with index 0 so we need to be specific on index so gave i-1)
				System.out.println("My Datasheet values are " + stringCellValue);
				
			} 
		wbook.close();
		
		}
		return data;
		
		
		

	}

}
